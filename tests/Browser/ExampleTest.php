<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends DuskTestCase
{
    use RefreshDatabase;
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testHomePage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Abstract Post');
        });
    }
    public function testAddComment()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->press('Add comment')
                    ->type('textarea', 'My comment')
                    ->press('Save')
                    ->waitForText('My comment')
                    ->assertSee('My comment');
        });
    }
    public function testUpdateComment()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->press('Add comment')
                    ->type('textarea', 'Comment to update')
                    ->press('Save')
                    ->waitForText('Comment to update')
                    ->press('Edit')
                    ->type('textarea', 'I want see another')
                    ->press('Update')
                    ->waitForText('I want see another')
                    ->assertSee('I want see another');
        });
    }
    public function testDeleteComment()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->press('Add comment')
                    ->type('textarea', 'This comment will be delete')
                    ->press('Save')
                    ->waitForText('This comment will be delete')
                    ->press('Delete')
                    ->pause(2000)
                    ->assertDontSee('This comment will be delete');
        });
    }
}
