<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->withHeaders([
            'accept' => 'application/json',
        ])->json('GET', '/api/comments');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
            ]);
    }
    public function testStore()
    {
        $response = $this->withHeaders([
            'accept' => 'application/json',
        ])->json('POST', '/api/comments', [
            'body' => 'test comment',
            'post_id' => 1,
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'body' => 'test comment',
                ],
            ]);
    }
    public function testDestroy()
    {
        $id = $this->withHeaders([
            'accept' => 'application/json',
        ])->json('POST', '/api/comments', [
            'body' => 'test comment',
            'post_id' => 1,
        ])->getOriginalContent()['data']['id'];
        $response = $this->withHeaders([
            'accept' => 'application/json',
        ])->json('DELETE', '/api/comments/' . $id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'success'
            ]);
    }
    public function testUpdate()
    {
        $id = $this->withHeaders([
            'accept' => 'application/json',
        ])->json('POST', '/api/comments', [
            'body' => 'test comment',
            'post_id' => 1,
        ])->getOriginalContent()['data']['id'];
        $response = $this->withHeaders([
            'accept' => 'application/json',
        ])->json('PUT', '/api/comments/' . $id, [
            'body' => 'another body'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'body' => 'another body'
                ]
            ]);
    }
}
