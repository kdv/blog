@extends('layouts.main')

@section('content')
    <post :post="{{json_encode($post)}}" :api_url="{{ json_encode(config('app.api_url')) }}"></post>
@endsection