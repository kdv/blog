<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\CommentUpdateRequest;
use App\Http\Resources\Comment;
use App\Repositories\CommentRepository;
use Illuminate\Http\Request;
use Validator;

/**
 * Class CommentController
 * @package App\Http\Controllers
 */
class CommentController extends Controller
{
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Comment::collection($this->commentRepository->orderBy('created_at', 'desc')->findWhere([
            'post_id' => $request->post_id,
            'parent_id' => null
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentStoreRequest $request)
    {
        $data = [
            'body' => $request->body,
            'post_id' => $request->post_id,
            'parent_id' => $request->has('parent_id') ? $request->parent_id : null
        ];
        $comment = $this->commentRepository->create($data);

        return new Comment($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new Comment($this->commentRepository->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentUpdateRequest $request, $id)
    {
        $this->idValidate($id);

        return new Comment($this->commentRepository->update(['body' => $request->body], $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->idValidate($id);

        $this->commentRepository->deleteWithChildren($id);

        return response()->json([
            'message' => 'success'
        ]);
    }

    /**
     * @param $id
     */
    private function idValidate($id)
    {
        Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => 'required|exists:comments,id'
            ]
        )->validate();
    }
}
