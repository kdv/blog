<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $post = [
            'id' => 1,
            'title' => 'Abstract Post',
            'body' => 'Leave a comment'
        ];

        return view('index', ['post' => $post]);
    }
}
